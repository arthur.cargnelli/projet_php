<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PollVoteRepository")
 */
class PollVote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="pollVotes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $polluser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Poll", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $poll;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PollOption", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $poll_option_vote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPolluser(): ?User
    {
        return $this->polluser;
    }

    public function setPolluser(?User $polluser): self
    {
        $this->polluser = $polluser;

        return $this;
    }

    public function getPollId(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getPollOptionVoteId(): ?PollOption
    {
        return $this->poll_option_vote;
    }

    public function setPollOptionVoteId(PollOption $poll_option_vote): self
    {
        $this->poll_option_vote = $poll_option_vote;

        return $this;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function getPollOptionVote(): ?PollOption
    {
        return $this->poll_option_vote;
    }

    public function setPollOptionVote(PollOption $poll_option_vote): self
    {
        $this->poll_option_vote = $poll_option_vote;

        return $this;
    }
}
