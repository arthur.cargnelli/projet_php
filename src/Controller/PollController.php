<?php

namespace App\Controller;

use App\Entity\Poll;
use App\Entity\User;
use App\Entity\PollVote;
use App\Form\PollType;
use App\Repository\PollVoteRepository;
use App\Repository\PollRepository;
use App\Repository\UserRepository;
use App\Repository\PollOptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/poll")
 */
class PollController extends AbstractController
{
    /**
     * @Route("/", name="poll_index", methods={"GET"})
     */

    public function index(PollRepository $pollRepository, PollOptionRepository $option): Response
    {
        $currentUser = $this->getUser();
        if($currentUser == NULL){
            $pollUser = $pollRepository->findall();
            $nb = [];
            foreach($pollUser as $x){ // Count all option of all poll when no users are logged in
                $nboption = $option->findby(array('poll_id'=>$x->getId()));
                $nb[$x->getId()] = count($nboption);
            }
            return $this->render('poll/index.html.twig', ['polls' => $pollRepository->findall(),'nboption' => $nb]);
        }
        $pollUser = $pollRepository->findby(array('poll_user_id'=>$currentUser->getId())); 
        $nb = [];
        foreach($pollUser as $x){ // Count all option of all poll's user logged in
            $nboption = $option->findby(array('poll_id'=>$x->getId()));
            $nb[$x->getId()] = count($nboption);
        }

        return $this->render('poll/index.html.twig', ['polls' => $pollRepository->findby(array('poll_user_id'=>$currentUser->getId())),'nboption' => $nb,'curentUser'=> $currentUser]);
    }

    /**
     * @Route("/new", name="poll_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poll = new Poll();
        $poll->setPollUser($this->getUser());
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poll);
            $entityManager->flush();

            return $this->redirectToRoute('poll_index');
        }

        return $this->render('poll/new.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
        ]);
    }

       /**
     * @Route("/{id}", name="poll_show", methods={"GET","POST"})
     */
    public function show(Poll $poll,PollOptionRepository $polloption, UserRepository $userRepo,Request $request,PollVoteRepository $pollVoteRepo): Response
    {
        $existe = false;
        $poll_option = $poll->getPollOptions();
        $creator = $userRepo->findBy(array('id'=> $poll->getPollUserId())); // name of the poll's creator
        $existPollVote = $pollVoteRepo->findBy(array('poll'=>$poll));
        if($this->getUser()){
            foreach($existPollVote as $pollvote){ // if the user already vote
                if($pollvote->getPollUser()->getId() == ($this->getUser()->getId())){
                    $existe = true;
                }
            }
        }
        $total = count($existPollVote);
        $nbVoteOption = [];
        if($total != 0){
            foreach($poll_option as $option) //calcul the poucentage of voted options and put it in a table
            {
                $tabOption = $pollVoteRepo->findBy(array('poll_option_vote'=>$option->getId()));
                $nbVoteOption[$option->getId()] = count($tabOption)*100/$total;
            }
        }
        else{
            foreach($poll_option as $option){ // if no vote for an option then O
                $nbVoteOption[$option->getId()] = 0;
            }
        }  
        $builder = $this->createFormBuilder();
            $choices = [];
            foreach ($poll->getPollOptions() as $option) {
                $choices[$option->getName()] = $option->getId();
            }
            $builder->add('choice', ChoiceType::class, [
                'label' => 'Votre réponse:',
                'choices' => $choices,
                'expanded' => true
            ]);
            $builder->add('ok', SubmitType::class, [
                'label' => 'Voter'
            ]);
            $form = $builder->getForm();
            $pvote = new PollVote();
            $pvote->setPoll($poll);
            $pvote->setPollUser($this->getUser());
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) { // save the form in DB
                
                    $pvote->setPollOptionVoteId($polloption->find($form->getdata()["choice"]));
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($pvote);
                    $entityManager->flush();

                    
                    return $this->render('poll/show.html.twig', [
                        'poll' => $poll,
                        'form' => $form->createView(),
                        'creator'=> $creator,
                        'nbVoteOption'=> $nbVoteOption,
                        'existe'=> $existe
     
                    ]);
            }
        return $this->render('poll/show.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
            'creator'=> $creator,
            'nbVoteOption'=> $nbVoteOption,
            'existe'=> $existe
            
        ]);
    }
    


    /**
     * @Route("/{id}/edit", name="poll_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Poll $poll): Response
    {
        $currentUser = $this->getUser();
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('poll_index', ['id' => $poll->getId()]);
        }

        return $this->render('poll/edit.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
            'user' => $currentUser,
        ]);
    }

    /**
     * @Route("/{id}", name="poll_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Poll $poll): Response
    {
        if ($this->isCsrfTokenValid('delete'.$poll->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($poll);
            $entityManager->flush();
        }

        return $this->redirectToRoute('poll_index');
    }
}
