<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Poll;
use App\Entity\User;
use App\Entity\PollVote;
use App\Form\PollType;
use App\Repository\PollVoteRepository;
use App\Repository\PollRepository;
use App\Repository\UserRepository;
use App\Repository\PollOptionRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PollRepository $pollRep)
    {
        $last = $pollRep->createQueryBuilder('p') // Search all poll order by id
        ->orderBy('p.id','DESC')
        ->getQuery();

        return $this->render('home/index.html.twig',['last'=> $last->execute()]);
    }
}
