<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181230214906 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE poll_vote (id INT AUTO_INCREMENT NOT NULL, polluser_id INT NOT NULL, poll_id_id INT NOT NULL, poll_option_vote_id_id INT NOT NULL, INDEX IDX_ED568EBEB258774D (polluser_id), UNIQUE INDEX UNIQ_ED568EBE19F5E396 (poll_id_id), UNIQUE INDEX UNIQ_ED568EBE73BE770E (poll_option_vote_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBEB258774D FOREIGN KEY (polluser_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBE19F5E396 FOREIGN KEY (poll_id_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBE73BE770E FOREIGN KEY (poll_option_vote_id_id) REFERENCES poll_option (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE poll_vote');
    }
}
