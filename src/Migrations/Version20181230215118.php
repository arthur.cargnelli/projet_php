<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181230215118 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE poll_vote DROP FOREIGN KEY FK_ED568EBE19F5E396');
        $this->addSql('ALTER TABLE poll_vote DROP FOREIGN KEY FK_ED568EBE73BE770E');
        $this->addSql('DROP INDEX UNIQ_ED568EBE19F5E396 ON poll_vote');
        $this->addSql('DROP INDEX UNIQ_ED568EBE73BE770E ON poll_vote');
        $this->addSql('ALTER TABLE poll_vote ADD poll_id INT NOT NULL, ADD poll_option_vote_id INT NOT NULL, DROP poll_id_id, DROP poll_option_vote_id_id');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBE3C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBEAA6EF690 FOREIGN KEY (poll_option_vote_id) REFERENCES poll_option (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED568EBE3C947C0F ON poll_vote (poll_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED568EBEAA6EF690 ON poll_vote (poll_option_vote_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE poll_vote DROP FOREIGN KEY FK_ED568EBE3C947C0F');
        $this->addSql('ALTER TABLE poll_vote DROP FOREIGN KEY FK_ED568EBEAA6EF690');
        $this->addSql('DROP INDEX UNIQ_ED568EBE3C947C0F ON poll_vote');
        $this->addSql('DROP INDEX UNIQ_ED568EBEAA6EF690 ON poll_vote');
        $this->addSql('ALTER TABLE poll_vote ADD poll_id_id INT NOT NULL, ADD poll_option_vote_id_id INT NOT NULL, DROP poll_id, DROP poll_option_vote_id');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBE19F5E396 FOREIGN KEY (poll_id_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE poll_vote ADD CONSTRAINT FK_ED568EBE73BE770E FOREIGN KEY (poll_option_vote_id_id) REFERENCES poll_option (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED568EBE19F5E396 ON poll_vote (poll_id_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ED568EBE73BE770E ON poll_vote (poll_option_vote_id_id)');
    }
}
